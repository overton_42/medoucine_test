<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PracticesController extends Controller
{
    
    /**
     * get all practices
     * @param Request $request
     * @return type
     */
    public function getAll(Request $request){

        $therapists = $this->load();
        $practices  = [];

        foreach($therapists as $therapist){
            foreach($therapist['practices'] as $practice){
                if(!in_array($practice, $practices, true)){
                    array_push($practices, $practice);
                }
            }
        }

        return $practices;
    }


    /**
     * Get a validator for an incoming request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $rules = [
            'city'      => ['required', 'string'],
            'practice'  => ['required', 'string'],
        ];

        return Validator::make($data, $rules);
    }

    /**
     * search therapists with a city/practice
     * @param Request $request
     * @return array
     */
    public function search(Request $request){

        $parameters = $this->validator($request->all())->validate();
        $therapists = $this->load();

        $filter = $therapists->filter(function($therapist, $key) use ($parameters){
            if ($therapist['city'] == $parameters['city'] && in_array($parameters['practice'], $therapist['practices'])) {
                return true;
            }
        });
         
        return $filter->all();
    }

}
