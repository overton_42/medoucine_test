<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * get all cities
     * @param Request $request
     * @return type
     */
    public function getAll(Request $request){
        $all    = $this->load();
        return $all->unique('city')->pluck('city');
    }


    /**
     * return a list of cities with their practices
     * @param Request $request
     * @return type
     */
    public function getPracticesByCities(Request $request){

        $therapists     = $this->load();
        $practices      = [];

        foreach($therapists as $therapist){
            foreach($therapist['practices'] as $practice){

                if(!isset($practices[$therapist['city']])){
                    $practices[$therapist['city']] = [
                        'name'      => $therapist['city'],
                        'practices' => []
                    ];
                }

                if(!in_array($practice, $practices[$therapist['city']]['practices'], true)){
                    array_push($practices[$therapist['city']]['practices'], $practice);
                }
            }
        }

        return $practices;
    }


}
