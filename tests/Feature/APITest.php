<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class APITest extends TestCase
{
    /**
     * valid request
     *
     * @return void
     */
    public function testCities()
    {
        $response = $this->get('/api/cities');
        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPractices()
    {
        $response = $this->get('/api/practices');
        $response->assertStatus(200);
    }

    /**
     * valid request
     *
     * @return void
     */
    public function testPracticesByCities()
    {
        $response = $this->get('/api/cities/practices');
        $response->assertStatus(200);
    }

    /**
     * valid search
     *
     * @return void
     */
    public function testSearch()
    {
        $response = $this->post('/api/search', [
            'city'     => 'Paris',
            'practice' => 'Feng Shui'
        ]);

        $response->assertStatus(200);
    }

    /**
     * Missing parameter
     *
     * @return void
     */
    public function testInvalidSearch()
    {
        $response = $this->post('/api/search', [
            'city'     => 'Paris',
        ]);

        $response->assertStatus(302);
    }

    
}
