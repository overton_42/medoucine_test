<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest extends DuskTestCase
{
    /**
     * Home
     *
     * @return void
     */
    public function testHome()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')->assertSee('Therapistes');
        });
    }

    /**
     * Home
     *
     * @return void
     */
    public function testSearch()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
            ->pause(1000)
            ->select('city', 'Paris')
            ->select('practice', 'Feng Shui')
            ->press('Envoyer')
            ->pause(1000)
            ->assertSee('Therapistes (1)')
            ->assertSee('Mr. Joany Tillman');
        });
    }
}
